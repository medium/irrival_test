import aiohttp
import asyncio
import json
import dateutil.parser
from datetime import datetime


from db import connect_to_pg, connect_to_redis
from .routes import routes as _routes

async def websocket_handler(request):
    app = request.app

    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)

    app['wslist'].add(ws)
    pg_pool = app['pg_pool']
    redis = app['redis_pool']

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            msg_dict = json.loads(msg.data)

            channel = msg_dict['channel']
            data = msg_dict['data']

            if channel == 'connect':
                driver_id = data['driver_id']

                async with pg_pool.acquire() as pg:
                    rows = await pg.fetch('SELECT * FROM messages WHERE driver_id = $1 ORDER BY created_at LIMIT 10', driver_id)
                    result = []

                    for row in [dict(x) for x in rows]:
                        result.append({
                            'id': row['id'],
                            'created_at': row['created_at'].isoformat(),
                            'message': row['message'],
                            'is_income': row['is_income']
                        })

                    await ws.send_json({
                        'channel': 'message_history',
                        'data': result
                    })

            if channel == 'message':
                driver_id = data['driver_id']

                current_timestamp = datetime.utcnow()
                message = (current_timestamp, data['message'], False, int(driver_id))

                print(message)

                async with pg_pool.acquire() as pg:
                    await pg.execute("""
                        INSERT INTO messages (created_at, message, is_income, driver_id)
                            VALUES ($1, $2, $3, $4)
                    """, *message)

                await redis.publish_json(f'message:{driver_id}:rcv', {
                    'message': data['message'],
                    'time': current_timestamp.isoformat()
                })

            if channel == 'exit':
                await ws.close()
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print(f'ws connection closed with exception {ws.exception()}')

    print('websocket connection closed')

    app['wslist'].discard(ws)

    return ws

async def connect_to_databases(app):
    print('Connecting to Postgres')
    app['pg_pool'] = await connect_to_pg()
    print('Connecting to Redis')
    app['redis_pool'] = await connect_to_redis()

    async def disconnect(app):
        await app['pg_pool'].close()
        app['redis_pool'].close()

    app.on_cleanup.append(disconnect)

async def background_listen_messages(app):
    pg_pool = app['pg_pool']
    redis = app['redis_pool']

    sub = await redis.psubscribe(f'message:*:snd')

    while True:
        channel, data = await sub[0].get_json()
        print(f"Received channel: {channel}, message: {data}")
        auth_id = await redis.get(f'auth:{data["token"]}')

        driver_id = str(channel).split(':')[1]

        if (not auth_id or int(driver_id) != int(auth_id)):
            await redis.publish_json(f'message:{driver_id}:rcv', { 'error': 'you are not authenticated' })
            continue

        try:
            message = (dateutil.parser.parse(data['time']), data['message'], True, int(driver_id))

            async with pg_pool.acquire() as pg:
                await pg.execute("""
                    INSERT INTO messages (created_at, message, is_income, driver_id)
                        VALUES ($1, $2, $3, $4)
                """, *message)

        except Exception as error:
            print('Error while persist message:', error)

        # Broadcast the message
        for ws in app['wslist']:
            await ws.send_json({
                'channel': 'message',
                'data': {
                    'driver_id': int(driver_id),
                    'time': data['time'],
                    'message': data['message']
                }
            })

async def start_listen_messages(app):
    app['background_listen_messages'] = app.loop.create_task(background_listen_messages(app))

async def shutdown_cleandup(app):
    await app['background_listen_messages']

def create_app():
    app = aiohttp.web.Application()
    app.add_routes(_routes)
    app.add_routes([aiohttp.web.get('/ws', websocket_handler)])
    app['wslist'] = set() 

    app.on_startup.append(connect_to_databases)
    app.on_startup.append(start_listen_messages)

    return app