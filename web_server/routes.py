from aiohttp import web
import secrets

routes = web.RouteTableDef()

@routes.get('/')
async def _root(request):
    """ Version root """
    return web.json_response({
        'version': [1, 0, 0]
    })

@routes.post('/drivers/auth')
async def _auth(request):
    """ Auth user """

    data = await request.post()
    driver_id = data['driver_id']
    token = secrets.token_hex(16)

    redis = request.app['redis_pool']

    await redis.set(f'auth:{token}', driver_id, expire=86400)

    return web.json_response({ 'data': token })

@routes.get('/drivers')
async def _drivers(request):
    """ List of drivers """

    pg_pool = request.app['pg_pool']

    result = []

    async with pg_pool.acquire() as pg:
        rows = await pg.fetch('SELECT * FROM drivers')

        for row in [dict(x) for x in rows]:
            result.append({
                'id': row['id'],
                'name': row['name'],
                'birthdate': str(row['birthdate']),
                'phone': row['phone'],
                'email': row['email']
            })

    return web.json_response({ 'data': result })