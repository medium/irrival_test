import asyncio
import random
import argparse
import aiohttp

from datetime import datetime
from faker import Faker
from db import connect_to_redis

fake = Faker('ru_Ru')

async def request_data(redis, token, driver_id):
    while True:
        data = {
            'token': token,
            'time': datetime.utcnow().isoformat(),
            'message': fake.sentence(nb_words=random.randint(5,15))
        }
        await redis.publish_json(f'message:{driver_id}:snd', data)

        sleep_delay = random.randint(1,5)
        await asyncio.sleep(sleep_delay)

async def subscribe_data(redis, driver_id):
    sub = await redis.subscribe(f'message:{driver_id}:rcv')

    while True:
        msg = await sub[0].get_json()
        print('Message received:')
        print(msg)

async def main(args):
    driver_id = args.driver_id
    token = ''

    async with aiohttp.ClientSession() as session:
        payload = { 'driver_id': driver_id }
        async with session.post('http://localhost:8080/drivers/auth', data=payload) as resp:
            result = await resp.json()
            token = result['data']

    print('Token received', token)

    redis = await connect_to_redis()

    tasks = [
        request_data(redis, token, driver_id),
        subscribe_data(redis, driver_id)
    ]

    await asyncio.gather(*tasks)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fake client for test application')
    parser.add_argument('driver_id', metavar='DRIVER_ID', type=int, help="user id")

    args = parser.parse_args()

    try:
        asyncio.run(main(args))
    except KeyboardInterrupt:
        pass