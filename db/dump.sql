DROP TABLE IF EXISTS drivers;

CREATE TABLE drivers
(
    id bigserial NOT NULL,
    name character varying(255) NOT NULL,
    birthdate date NOT NULL,
    phone character varying(20) NOT NULL,
    email character varying(100),
    PRIMARY KEY (id)
);

INSERT INTO drivers (name, birthdate, phone, email)
    VALUES
    ('Пономарева Иванна Максимовна', '1990-01-01', '+77839834800', 'kornil_1997@rao.com'),
    ('Кузьмина Анжелика Афанасьевна', '1990-01-01', '8 393 732 92 80', 'ershovaija@mail.ru'),
    ('Савин Павел Феоктистович', '1990-01-01', '8 339 821 4078', 'zinovikulagin@npo.ru'),
    ('Сорокин Савватий Валерианович', '1990-01-01', '8 027 976 8615', 'anisim_58@yahoo.com'),
    ('Воробьев Святослав Анисимович', '1990-01-01', '8 620 885 4348', 'agapsuvorov@rambler.ru'),
    ('Мамонтова Лариса Юльевна', '1990-01-01', '+7 (857) 044-5757', 'marfa_97@mail.ru'),
    ('Комиссаров Викторин Герасимович', '1990-01-01', '8 637 246 49 92', 'militsa1991@yahoo.com'),
    ('Владимиров Самуил Евстигнеевич', '1990-01-01', '+7 (619) 686-76-10', 'julian_2009@npo.com'),
    ('Самсонова Валентина Викторовна', '1990-01-01', '82586976686', 'antonin_2014@danilova.net'),
    ('Гордеев Твердислав Антонович', '1990-01-01', '8 040 326 80 58', 'efimovanatalja@ip.edu'),
    ('Карпова Агата Афанасьевна', '1990-01-01', '8 (326) 652-4130', 'iljasuvorov@npo.info'),
    ('Беляева Фёкла Антоновна', '1990-01-01', '+7 (884) 581-4651', 'savva39@rao.com'),
    ('Соболева Валентина Викторовна', '1990-01-01', '+7 (162) 446-75-64', 'ferapontivanov@rambler.ru'),
    ('Харитонова Наталья Феликсовна', '1990-01-01', '+7 081 790 9868', 'ipatisorokin@mail.ru'),
    ('Шилова Ирина Григорьевна', '1990-01-01', '8 (157) 283-7687', 'maksimiljansergeev@ribakova.com'),
    ('Кудрявцева Светлана Львовна', '1990-01-01', '+7 (574) 157-3766', 'anike_00@hotmail.com'),
    ('Лихачева Иванна Георгиевна', '1990-01-01', '8 479 864 10 04', 'nikitinemmanuil@yandex.ru'),
    ('Мамонтов Эмиль Владленович', '1990-01-01', '8 (503) 037-6199', 'ereme1982@hotmail.com'),
    ('Беляева Виктория Руслановна', '1990-01-01', '+77847745488', 'smirnovavalentina@hotmail.com'),
    ('Шашков Харлампий Валентинович', '1990-01-01', '8 (324) 111-8254', 'efim_82@hotmail.com');

DROP TABLE IF EXISTS messages;

CREATE TABLE messages
(
    id bigserial NOT NULL,
    created_at timestamp without time zone NOT NULL,
    message text COLLATE pg_catalog."default",
    driver_id bigint NOT NULL,
    is_income boolean NOT NULL DEFAULT true,
    PRIMARY KEY (id)
);

-- DROP INDEX public.messages_created_at_index;

CREATE INDEX messages_created_at_index ON messages USING btree (created_at);