import asyncpg
import aioredis
import os

PG_DSN = os.getenv('PG_DSN', 'postgresql://postgres@localhost:5432/irrival_test')
REDIS_DSN = os.getenv('REDIS_DSN', 'redis://localhost:6379/0')

async def connect_to_pg():
    try:
        conn = await asyncpg.create_pool(PG_DSN)

        return conn
    except Exception as error:
        print(f"Failed connect to postgres")
        raise

async def connect_to_redis():
    try:
        conn = await aioredis.create_redis_pool(REDIS_DSN)
        
        return conn
    except Exception as error:
        print(f"Failed connect to redis")
        raise
