FROM python:3.7.4-alpine

RUN apk add --no-cache \
    postgresql-dev \
    python3-dev \
    gcc \
    libc-dev \
    bind-tools

RUN pip install --upgrade pip

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r /app/requirements.txt

COPY . /app
WORKDIR /app

CMD ["python3","./main.py"]
